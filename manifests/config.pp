# @summary
#   This class handles the configuration file's.
#
# @api private
#
class phpmyadmin::config inherits phpmyadmin {

  file { '/etc/phpmyadmin/apache.conf':
    ensure  => file,
    content => epp('phpmyadmin/phpmyadmin.conf.epp'),
    notify  => Service['apache2'],
  }

  file { '/etc/apache2/conf-available/phpmyadmin.conf':
    ensure => 'link',
    target => '/etc/phpmyadmin/apache.conf',
  }

  file { '/etc/apache2/conf-enabled/phpmyadmin.conf':
    ensure => 'link',
    target => '/etc/apache2/conf-available/phpmyadmin.conf',
    notify => Service['apache2'],
  }

}
