# @summary
#   This class handles phpmyadmin packages.
#
# @api private
#
class phpmyadmin::install inherits phpmyadmin {

  package { $phpmyadmin::package_name:
    ensure => installed,
  }

}
