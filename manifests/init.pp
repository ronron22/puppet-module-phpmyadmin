# phpmyadmin
#
# @summary Init class for phpmyadmin module, includes other classes
#
# @example Declare this class in site.pp
#   class phpmyadmin {
#   configuration_directory => '/usr/local/phpmyadmin/etc/phpmyadmin',
#   package_name            => phpmyadmin',
#   service_name            => 'phpmyadmin',
#   }
#
# @param package_name
#    Package name. Default value: undef.
#
# @param authldap
#   Enable or not ldap auth. Default value: undef.
#
# @param authldapurl
#   ldap server url. Default value: undef.
#
# @param authtype 
#   Auth type ex basic. Default value: undef.
#
# @param authname
#   Auth type ex basic. Default value: undef.
#
# @param authbasicprovider
#   Provider ex ldap  Default value: undef.
#
# @param authuserfile
#   Important, otherwise "(9)Bad file descriptor: Could not open password file: (null). Default value: undef.
#
# @param requirewhat
#   Require what ? ex valid-user. Default value: undef.
#
class phpmyadmin (

  ## templating
  Optional[String]  $package_name = undef,
  Optional[Boolean] $authldap = undef,
  Optional[String]  $authldapurl = undef,
  Optional[String]  $authtype = undef,
  Optional[String]  $authname = undef,
  Optional[String]  $authbasicprovider = undef,
  Optional[String]  $authuserfile = undef,
  Optional[String]  $requirewhat = undef,

  ) {

  anchor { 'phpmyadmin::begin': } -> class { '::phpmyadmin::install': } -> class { '::phpmyadmin::config': } ~> anchor { 'phpmyadmin::end': }

}
